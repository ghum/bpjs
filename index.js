import { ask, readLine } from 'stdio';
import axios from 'axios';
import btoa from 'btoa';
import CryptoJS from 'crypto-js';
import Base64 from 'base-64';
import LZString from 'lz-string';
import Browser from 'zombie';
import terminalImage from 'terminal-image';
import cliProgress from 'cli-progress';

let pass = 'b!JtC$!k7RL&aE!pt8%a-=xnJz7H4q3nVGwx5?49*ewPcC3Q@yQTuKVPf_f!y+$DPtXR$R?QB&C#!rr$K!@QZ9Zh8QMRQbDjSRBASjR@_C+w8Nq^+gQr-Vd47-Bm52Xu',
  username = 'Selfiemarlina', secret = 'Bpjs2022@',
  BotDetect, _p, _m, _bm, Id, InstanceId, uriCaptcha, cookies = [], verificationToken = '',
  uriLogin = 'https://pcarejkn.bpjs-kesehatan.go.id/eclaim/Login',
  cmd, browser = new Browser(),
  progress = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);

const encrypt = msg => {
  msg = Base64.encode(msg);
  return CryptoJS.AES.encrypt(msg, pass).toString();
}

const CC2C = (a, b) => {
  b = b.split("");
  var c = b.length
    , m = a.split("")
    , d = "";
  for (a = a.length - 1; 0 <= a; a--) {
    var n = b[c-- - 1];
    n = "undefined" !== typeof n && "1" === n ? m[a].toUpperCase() : m[a].toLowerCase();
    d = n + d
  }
  return d
}

const getNoKartu = async (nik, type = 'D', json = false) => {
  // console.log(nik, type, json)
  let collect = [];
  progress.start(nik.length, 0);
  for (let index = 0; index < nik.length; index++) {
    await axios('https://pcarejkn.bpjs-kesehatan.go.id/eclaim/EntriDaftarDokkel/getListPesertaDetail?id=1&noka=' + nik[index] + '&kode=',
      {
        method: 'get',
        headers: {
          'Cookie': cookies
        },
      })
      .then(response => {
        let r = null;
        if (response.data.hasOwnProperty("response")) {
          if (response.data.response.list.length > 0) {
            const d = response.data.response.list[0];
            if (type === 'N') {
              r = d.noKartu;
            } else {
              r = {
                noKartu: d.noKartu,
                nik: d.nik,
                nama: d.nama,
                jenisKelamin: d.sex,
                tglLahir: d.tglLahir,
                alamat: '',
                statusKawin: d.statusKawin.nama,
                statusAktif: d.statusAktif.nama,
                faskes: d.kdPpkPst.nmPPK,
                noHP: d.noHP,
              };
            }
          } else {
            if (type === 'N') {
              r = null;
            } else {
              r = {
                noKartu: '',
                nik: nik[index],
                nama: '',
                jenisKelamin: '',
                tglLahir: '',
                alamat: '',
                statusKawin: '',
                statusAktif: '',
                faskes: '',
                noHP: '',
              };
            }
          }
          collect = [
            ...collect,
            r
          ];
        }
        progress.update(index);
      })
      .catch(error => {
        console.error('getNoKartu.error:', error);
      });

  }
  progress.stop();
  console.log(json ? JSON.stringify(collect) : collect);
}

const goToLogin = () => {
  return new Promise((resolve, reject) => {
    browser.visit(uriLogin, async () => {
      // console.log('browser.html:', browser.html('.tab-content'));
      verificationToken = browser.querySelector('input[name="__RequestVerificationToken"]').value;
      BotDetect = browser.evaluate('$("#CaptchaInputText").get(0).Captcha');
      _p = BotDetect._p;
      _m = BotDetect._m;
      _bm = BotDetect._bm;
      Id = BotDetect.Id;
      InstanceId = BotDetect.InstanceId;

      /* console.log('_p:', BotDetect._p);
      console.log('_m:', BotDetect._m);
      console.log('_bm:', BotDetect._bm.toString());
      console.log('Id:', BotDetect.Id);
      console.log('InstanceId:', BotDetect.InstanceId); */
      /* console.log('CM:', CM(_p));
      console.log('CInt2B:', CInt2B(CM(_p))); */

      cookies[0] = 'ASP.NET_SessionId=' + browser.getCookie('ASP.NET_SessionId') + '; Path=/; Secure; HttpOnly; Expires=' + new Date(new Date().getTime() + 86400000).toUTCString() + ';';
      cookies[1] = 'BIGipServerpool_pcarejkn=' + browser.getCookie('BIGipServerpool_pcarejkn') + '; Path=/; Secure; HttpOnly;'
      cookies[2] = 'TS01936ab2=' + browser.getCookie('TS01936ab2') + '; Path=/;'
      cookies[3] = '__RequestVerificationToken_L2VjbGFpbQ2=' + browser.getCookie('__RequestVerificationToken_L2VjbGFpbQ2') + '; Path=/; HttpOnly;'
      cookies[4] = 'f5avraaaaaaaaaaaaaaaa_session_=' + browser.getCookie('f5avraaaaaaaaaaaaaaaa_session_') + '; Path=/eclaim; Secure; HttpOnly;'

      uriCaptcha = BotDetect.ImageSrcUrl;
      await axios(uriCaptcha, {
        responseType: 'arraybuffer', headers: {
          'Cookie': cookies
        }
      })
        .then(async response => {
          try {
            console.log(await terminalImage.buffer(response.data));
            const captcha = await ask('Captcha');
            const password = secret === null ? await ask('Password') : null;

            let newCaptcha = CC2C(captcha, _bm.toString());
            // console.log('newCaptcha:', captcha);

            let e = {
              username: username,
              password: btoa(secret === null ? password : secret),
              CaptchaInputText: newCaptcha,
              captchaid: Id,
              instanceid: InstanceId
            };
            let o = LZString.compressToEncodedURIComponent(JSON.stringify(e));

            /* console.log('LZStringJSON:', JSON.stringify(e))
            console.log('__RequestVerificationToken:', verificationToken);
            console.log('mixed:', encrypt(o)); */

            await axios(uriLogin + '/login',
              {
                method: 'post',
                headers: {
                  'Content-Type': 'multipart/form-data',
                  'Cookie': cookies
                },
                data: {
                  __RequestVerificationToken: verificationToken,
                  mixed: encrypt(o)
                },
              })
              .then(async response => {
                console.log('Login Berhasil!');
                resolve(response.data);
              })
              .catch(error => {
                console.error('login.error:', error);
              });
          } catch (err) {
            console.error('captcha.error:', err);
          };
        })
        .catch(error => {
          console.error('error:', error);
        });
    });
  });
}

const terminal = async () => {
  const nik = await ask('NIK');
  const type = await ask('Tipe');
  const json = await ask('Json');
  const NIK = nik.split(' ');
  getNoKartu(NIK, (type === '' ? 'D' : 'N'), (json === 'Y' ? true : false));
}

const main = () => {
  goToLogin().then(async () => {
    await terminal();

    do {
      cmd = await readLine();
      if (cmd === 'login') {
        browser.window.close();
        browser = new Browser();
        main();
      } else if (cmd === 'cek') {
        await terminal();
      } else if (cmd === 'exit' || cmd === 'close') {
        browser.window.close();
        await readLine({ close: true });
      }
    } while (cmd !== 'exit' && cmd !== 'close');
  })
}

main();